﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Aspnetcoreproject.Helpers;
using Aspnetcoreproject.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Aspnetcoreproject
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            
        }

        public IConfiguration Configuration { get; }


        public void ConfigureAppServices(IServiceCollection services)
        {
            services.AddScoped<IUserService, UserService>();
          
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            this.ConfigureAppServices(services);


            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
         .AddJwtBearer(x =>
         {
             x.RequireHttpsMetadata = false;
             x.SaveToken = true;
             x.TokenValidationParameters = new TokenValidationParameters
             {
                 ValidateIssuerSigningKey = true,
                 IssuerSigningKey = new SymmetricSecurityKey(key),
                 ValidateIssuer = false,
                 ValidateAudience = false
             };
         });


            services.AddCors();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1",
           new OpenApiInfo
           {
               Version = "v1",
               Title = "v1 API",
               Description = "v1 API Description"
           });

                // Add a SwaggerDoc for v2 
                //options.SwaggerDoc("v2",
                //    new OpenApiInfo
                //    {
                //        Version = "v2",
                //        Title = "v2 API",
                //        Description = "v2 API Description"
                //    });

                // Apply the filters
                //options.OperationFilter<RemoveVersionFromParameter>();
                //options.DocumentFilter<ReplaceVersionWithExactValueInPath>();
                

                // Ensure the routes are added to the right Swagger doc
                //options.DocInclusionPredicate((docName, description) => {
                //    if (!description.TryGetMethodInfo(out MethodInfo methodInfo)) return false;
                //    var versions = methodInfo.DeclaringType
                //    .GetCustomAttributes(true)
                //    .OfType<ApiVersionAttribute>()
                //    .SelectMany(attr => attr.Versions);


                //    // Ensure the routes are added to the right Swagger doc
                //    var maps = methodInfo.DeclaringType
                //    .GetCustomAttributes(true)
                //    .OfType<ApiVersionAttribute>()
                //    .SelectMany(attr => attr.Versions)
                //    .ToArray();

                //    return versions.Any(v => $"v{v.ToString()}" == docName)
                //    & (!maps.Any() || maps.Any(v => $"v{v.ToString()}" == docName));
                //});



            });


         
            services.AddApiVersioning(
                o=> {
                o.AssumeDefaultVersionWhenUnspecified = true;
                o.DefaultApiVersion = new ApiVersion(1, 0);
            }
                );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(x => x
             .AllowAnyOrigin()
             .AllowAnyMethod()
             .AllowAnyHeader()); 

            app.UseAuthentication();

            

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My Api");
                // Specify and endpoint for v2
                //c.SwaggerEndpoint($"/swagger/v2/swagger.json", $"v2");

            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
