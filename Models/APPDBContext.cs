﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aspnetcoreproject.Models
{
    public class APPDBContext:DbContext
    {
        public DbSet<Orderdetail> OrderDetails { get; set; }
        public DbSet<Orders> Orders { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<AuthorBiography> authorBiographies { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=DESKTOP-569KLJ7;Initial Catalog=StoreDB;Trusted_Connection=True");
        }


    }
}
